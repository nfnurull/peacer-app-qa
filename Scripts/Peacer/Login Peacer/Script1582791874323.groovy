import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('C:\\Users\\nurul.fitri\\Downloads\\app-debug (19).apk', true)

WebUI.delay(3)

Mobile.tap(findTestObject('Positive Login Peacer/android.widget.Button0 - Introduction (1)'), 0)

Mobile.swipe(350, 700, 50, 650)

Mobile.swipe(350, 700, 10, 650)

Mobile.tap(findTestObject('Positive Login Peacer/android.widget.Button0 - Get Started (2)'), 0)

Mobile.setText(findTestObject('Positive Login Peacer/android.widget.EditText0 - Email Address (1)'), 'nfnurull@gmail.com', 
    0)

Mobile.setText(findTestObject('Positive Login Peacer/android.widget.EditText0 (1)'), 'kalkulus', 0)

Mobile.tap(findTestObject('Positive Login Peacer/android.widget.Button0 - Sign in (1)'), 0)

Mobile.verifyElementText(findTestObject('Positive Login Peacer/android.widget.TextView0 - LEA'), 'LEA')

Mobile.closeApplication()

