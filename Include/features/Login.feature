Feature: Login feature

  Scenario Outline: Test login with registered credentials
    Given User navigates to sign in page
    When User input <email> and <password>
    And Click on Sign in button
    Then User is navigated to homepage
    
    Examples:
    |      email                 | password |
    | azizmustikaaji@gmail.com   | kalkulus |
    | peacer.apps@gmail.com      | kalkuluss|