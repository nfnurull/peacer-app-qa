import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class LoginStepsPositive {
	@Given("User navigates to sign in page")
	def navigateToLoginPage(){
		Mobile.startApplication('C:\\Users\\nurul.fitri\\Downloads\\app-debug (19).apk', true)

		WebUI.delay(3)

		Mobile.tap(findTestObject('Positive Login Peacer/android.widget.Button0 - Introduction (1)'), 0)

		Mobile.swipe(350, 700, 50, 650)

		Mobile.swipe(350, 700, 50, 650)

		Mobile.tap(findTestObject('Positive Login Peacer/android.widget.Button0 - Get Started (2)'), 0)
	}

	@When("User input (.*) and (.*)")
	def enterCredentials(String email, String password){
		println ("Email : "+email)
		println ("Password : "+password)

		Mobile.setText(findTestObject('Positive Login Peacer/android.widget.EditText0 - Email Address'), email, 0)

		Mobile.setText(findTestObject('Positive Login Peacer/android.widget.EditText0'), password, 0)
	}

	@And("Click on Sign in button")
	def clickSignin(){
		Mobile.tap(findTestObject('Positive Login Peacer/android.widget.Button0 - Sign in (1)'), 0)
	}

	@Then("User is navigated to homepage")
	def verifyHomepage(){
		Mobile.verifyElementText(findTestObject('Positive Login Peacer/android.widget.TextView0 - LEA'), 'LEA')

		Mobile.closeApplication()
	}
}